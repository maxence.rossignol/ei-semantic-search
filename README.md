# EI Semantic Search

In order to get the search engine to work, open the Search_Engine.ipynb (rather on Colab).

Then execute the init code that can be found throughout the page

The steps to follow are :
- Import libraries and personalize global variables
- Extract data
- Initialising the semantic model ('all-MiniLM-L6-v2')
- Compute and save, (or load with pickle) the embeddings needed for cosine_similarity computations
- If not initialized, compute the most probable topics of each doc, otherwise load it with pickle
- Now enjoy the make_query function